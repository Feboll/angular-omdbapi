angular.module('omdbForm', []);

angular.module('omdbForm').component('omdbForm', {
    templateUrl: 'code/omdb-form/omdb-form.template.html',
    controller: ['$scope', '$http', function BookListController($scope, $http) {
        var self = this;

        self.result = [];
        self.title = '';
        self.year = '';
        self.type = '';

        self.totalItems = 0;
        self.currentPage = 1;

        self.pageChanged = function () {
            self.getResult(self.title, self.year, self.type, self.currentPage);
        };

        self.maxSize = 5;

        $scope.submit = function () {
            self.getResult(self.title, self.year, self.type, self.currentPage);
        };

        self.getResult = function (title, year, type, page) {
            var serchRequest = 'http://www.omdbapi.com/?s=' + title + '&y=' + year + '&type=' + type + '&page=' + page;

            $http.get(serchRequest).then(function (response) {
                self.result = response.data.Search;
                self.totalItems = response.data.totalResults;
            });
        }
    }]
});

angular.module('omdbForm').filter('checkImage', function () {
    return function (input) {
        var imageUrl = "assets/img/default.jpg";
        
        if (input !== "N/A") {
            imageUrl = input;
        }

        console.info(imageUrl);


        return imageUrl;
    }
});