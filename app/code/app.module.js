angular.module('omdbApp', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'omdbForm']);


angular.module('omdbApp').directive('myValidation', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, mCtrl) {
      function myValidation(value) {
        if (value.length > 3) {
          mCtrl.$setValidity('small', true);
        } else {
          mCtrl.$setValidity('small', false);
        }
        return value;
      }
      mCtrl.$parsers.push(myValidation);
    }
  };
})